" gofmt uses tabs, len arbitrary
" no set line len
setlocal noexpandtab
setlocal shiftwidth=2
setlocal tabstop=2
" needed to fix the color issue on buffer modify
"autocmd! BufWritePost <buffer> call lightline#highlight()
"autocmd! BufEnter <buffer> call lightline#highlight()
"au Filetype go nmap <leader>gr :GoDeclsDir<cr>
"let g:go_auto_type_info = 1
