" Indent Settings
setlocal expandtab
setlocal tabstop=4
setlocal shiftwidth=4
setlocal textwidth=80
setlocal smarttab
let &colorcolumn=join(range(79,80),",")

" neoformat - Autoformat Utility Settings {{{
" add a check for yapf binary
"echoerr 'missing yapf'

let g:neoformat_python_yapf= {
            \ 'exe': 'yapf',
            \ 'args': [],
            \ 'replace': 0,
            \ 'stdin': 1,
            \ 'valid_exit_codes': [0],
            \ 'no_append': 0,
            \ 'stderr': 1,
            \ 'error_hint': 1
            \ }

let g:neoformat_enabled_python = ['yapf']

augroup fmt
  autocmd!
  autocmd BufWritePre * Neoformat
augroup END

" }}}
