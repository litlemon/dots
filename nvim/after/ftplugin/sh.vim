setlocal noexpandtab
setlocal shiftwidth=2
setlocal tabstop=2
setlocal textwidth=80
let &colorcolumn=join(range(79,80),",")

