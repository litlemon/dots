setlocal foldmethod=marker
setlocal expandtab
setlocal shiftwidth=4
setlocal tabstop=4
autocmd! BufWritePost <buffer> call lightline#highlight()

