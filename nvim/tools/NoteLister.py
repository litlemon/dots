import os, sys, re
# import pdb
# projects you are actively working on add workspace package name that you want the lister to look through
# forces a limited search of workspace files to make plugin faster
PROJECTLIST = []
pwd = sys.argv[1]

import subprocess
try:
    csvRoot = subprocess.check_output(['git', 'rev-parse', '--show-toplevel'])
except subprocess.CalledProcessError:
    path = ""
else:
    path = csvRoot
## can replace with grep or find ( need to return list of files )
# generates a list of full paths to files in specified packages only
# ctrlp greps through this list to find the specified file
#cmd = 'ag -l -g "" ' + path
#cmd = 'rg --files -g "" ' + path
# must decode to str for py3 compat
path = path.decode('utf-8')
# currently want until root found not only from current dir
cmd = 'fd . -e md .'  # % (path.strip(), path.strip())
os.system(cmd)
