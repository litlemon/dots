" ---------------------------------
"  Plugin Manager Settings
" ---------------------------------

" plugin loading
call plug#begin('~/.vim/plugged')

" Utils {{{

" commentor
"Plug 'tpope/vim-commentary', { 'on' : 'Commentary' }

" ALE
"Plug 'dense-analysis/ale'
"exec 'source ' . $HOME . '/.vim/rc/plugins/ale.vim'

" ctag manager
" settings file: plugins/ctags.vim
"Plug 'ludovicchabant/vim-gutentags'
"exec 'source ' . $HOME . '/.vim/rc/plugins/ctags.vim'

" echodocs - function prototype doc autocomplete
" settings file: plugins/echodoc.vim
Plug 'Shougo/echodoc.vim'
exec 'source ' . $HOME . '/.vim/rc/plugins/echodoc.vim'

" fzf - fuzzy finder
" settings file: plugins/fzf.vim
"Plug 'junegunn/fzf.vim', { 'on' : ['Files', 'Rg', 'Tags', 'Buffers', 'GFiles', 'BTags'] }
Plug 'junegunn/fzf.vim'
exec 'source ' . $HOME . '/.vim/rc/plugins/fzf.vim'

" goyo - focus mode
Plug 'junegunn/goyo.vim', { 'on' : 'Goyo' }


" Tabular - table formatter
Plug 'godlygeek/tabular', { 'on' : 'Tab' }

" deoplete - autocomplete
" settings file: plugins/deoplete.vim
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
exec 'source ' . $HOME . '/.vim/rc/plugins/deoplete.vim'

" }}}

" UI Plugins {{{

" color scheme: Gruvbox --- default color
Plug 'morhetz/gruvbox'
Plug 'joshdick/onedark.vim'

" status bar
function! StatusBarSetup() 
    set laststatus=2
    if !has( 'gui_running' )
        set t_Co=256
    endif
endfunction
Plug 'itchyny/lightline.vim', { 'do' : function('StatusBarSetup') }

" buffer bar
Plug 'ap/vim-buftabline'

" Tag Navigator
Plug 'majutsushi/tagbar'
nnoremap <Leader>gt :TagbarToggle<CR>

" File Navigator
Plug 'scrooloose/nerdtree', { 'on' : 'NERDTreeToggle' }
map <Leader>gf :NERDTreeToggle<CR>

" Git management
Plug 'tpope/vim-fugitive'

" }}}

" LANG {{{
" Language specific plugins, mostly syntax and ftdetecting

" PYTHON
" ------

" GOLANG
" ------
" ft settings: ../after/ftplugin/go.vim
function! GoCfgSetup()
    let g:go_def_mapping_enabled = 0
endfunction
"Plug 'fatih/vim-go', { 'for' : 'go', 'do' : function('GoCfgSetup') }
Plug 'fatih/vim-go', { 'for' : 'go' }

Plug 'deoplete-plugins/deoplete-go', { 'for':['go'], 'do': 'make'}
"Plug 'deoplete-plugins/deoplete-go'

" JAVASCRIPT/TYPESCRIPT/REACT
" ---------------------------
" post install (yarn install | npm install) then load plugin only for editing supported files
" ft settings: ../after/ftplugin/javascript.vim
" ft settings: ../after/ftplugin/typescript.vim
" ft settings: ../after/ftplugin/yaml.vim
Plug 'prettier/vim-prettier', {
  \ 'do': 'yarn install',
  \ 'for': [
  \          'javascript',
  \          'javascriptreact',
  \          'typescript',
  \          'typescriptreact',
  \          'css',
  \          'less',
  \          'scss',
  \          'json',
  \          'graphql',
  \          'markdown',
  \          'vue',
  \          'yaml',
  \          'html'] }

Plug 'kristijanhusak/vim-js-file-import', {
  \ 'for': [
  \          'javascript',
  \          'typescript',
  \          'css',
  \          'less',
  \          'scss',
  \          'json',
  \          'graphql',
  \          'markdown',
  \          'vue',
  \          'yaml',
  \          'html'] }

Plug 'pangloss/vim-javascript', { 'for': 'javascript' }
Plug 'MaxMEllon/vim-jsx-pretty', { 'for': 'javascriptreact' }
Plug 'leafgarland/typescript-vim', { 'for': 'typescript' }
Plug 'peitalin/vim-jsx-typescript', { 'for': 'typescriptreact' }
Plug 'jparise/vim-graphql', { 'for': 'graphql' }
Plug 'styled-components/vim-styled-components', { 'branch': 'main' }

" }}}

call plug#end()
