" ale configuration file

" disable continuously bg linting
"let g:ale_lint_on_text_changed = 'never'

" disable lint on file open
"let g:ale_lint_on_enter = 0

let g:ale_linters = {
\   'python': [],
\}

let g:ale_fixers = {
\   'python': ['yapf'],
\}

"let g:ale_linters = {
"\   'javascript': ['eslint'],
"\   'typescript': ['tsserver', 'tslint'],
"\   'vue': ['eslint'],
"\   'python': [],
"\}
"
"let g:ale_fixers = {
"\    'javascript': ['eslint'],
"\    'typescript': ['prettier'],
"\    'vue': ['eslint'],
"\    'scss': ['prettier'],
"\    'html': ['prettier'],
"\    'python': []
"\}
