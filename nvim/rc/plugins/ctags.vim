" define a custom file to force trigger tags
let g:gutentags_project_root = ['.gutctags' ]
"let g:gutentags_trace = 1
if executable('fd')
  let g:gutentags_file_list_command = 'python ~/.vim/tools/FzfLister.py $(pwd)'
endif
