" -----------------------------
"  Initialization Settings
" -----------------------------

filetype on
let g:python3_host_prog = $HOME . '/.venv/nvim/bin/python3'

" Disable Default plugins
let g:loaded_2html_plugin      = 0
let g:loaded_logiPat           = 0
let g:loaded_getscriptPlugin   = 1
let g:loaded_gzip              = 0
let g:loaded_man               = 1
let g:loaded_matchit           = 1
let g:loaded_matchparen        = 1
"let g:loaded_netrwFileHandlers = 1
"let g:loaded_netrwPlugin       = 1
"let g:loaded_netrwSettings     = 1
let g:loaded_rrhelper          = 0
"let g:loaded_shada_plugin      = 1
"let g:loaded_spellfile_plugin  = 1
let g:loaded_tarPlugin         = 0
let g:loaded_tutor_mode_plugin = 0
let g:loaded_vimballPlugin     = 0
let g:loaded_zipPlugin         = 0
