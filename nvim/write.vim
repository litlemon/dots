filetype on
let g:python3_host_prog = $HOME . '/.venv/nvim/bin/python3'

" Plugins
call plug#begin('~/.vim/plugged')
Plug 'foxbunny/vim-amber'
Plug 'eemed/sitruuna.vim'
Plug 'morhetz/gruvbox'
Plug 'sonph/onehalf'
"Plug 'joshdick/onedark.vim'

" Git management
Plug 'tpope/vim-fugitive'

" buffer bar
Plug 'ap/vim-buftabline'

" status bar
function! StatusBarSetup() 
    set laststatus=2
    if !has( 'gui_running' )
        set t_Co=256
    endif
endfunction
Plug 'itchyny/lightline.vim', { 'do' : function('StatusBarSetup') }

Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
exec 'source ' . $HOME . '/.vim/rc/plugins/deoplete.vim'

" Tag Navigator
Plug 'majutsushi/tagbar'
nnoremap <Leader>gt :TagbarToggle<CR>

Plug 'ron89/thesaurus_query.vim'

" amazing plugin, works how you think spell check should work
" opens a special mode where you're checking spelling
" n => Next
" N => previous
" j,k directional
" enter => select suggested spelling
Plug 'nobe4/vimcorrect'
Plug 'junegunn/goyo.vim'
Plug 'junegunn/fzf.vim'
Plug 'npxbr/glow.nvim', {'do': ':GlowInstall', 'branch': 'main'}
Plug 'https://github.com/alok/notational-fzf-vim'
call plug#end()

color gruvbox
"color sitruuna
"color amber
"color onedark
let g:lightline = {
            \ 'colorscheme': 'sitruuna',
            \ }
let g:nv_search_paths = ['~/Documents/writing']

set termguicolors
set ignorecase

" http://owen.cymru/fzf-ripgrep-navigate-with-bash-faster-than-ever-before/
" define the backend func to search through files using rg
exec 'source ' . $HOME . '/.fzf/plugin/fzf.vim'
let g:rg_command = '
  \ rg --column --line-number --no-heading --fixed-strings --ignore-case --no-ignore --hidden --follow --color "always"
  \ -g "*.{js,json,php,md,styl,jade,html,config,py,cpp,c,go,hs,rb,conf}"
  \ -g "!{.git,node_modules,vendor}/*" '

" creates a new command :F to grep through files using rg
command! -bang -nargs=* F call fzf#vim#grep(g:rg_command .shellescape(<q-args>), 1, <bang>0)

" define custom logic for FZF / Files w/ctrl-p trigger

" need to modify
nnoremap <C-p> :call fzf#run( {'source':'python ~/.vim/tools/NoteLister.py $(pwd)', 'sink' : 'e'})<CR>
nnoremap <Leader>b :Buffers<CR>

nnoremap <Leader>gd :call fzf#vim#tags(expand('<cword>'))<CR>

" easier swap between splits
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

nnoremap j gj
nnoremap k gk

" replaces z= with a hook to fzf instead of new buffer
function! FzfSpellSink(word)
  exe 'normal! "_ciw'.a:word
endfunction
function! FzfSpell()
  let suggestions = spellsuggest(expand("<cword>"))
  return fzf#run({'source': suggestions, 'sink': function("FzfSpellSink"), 'down': 10 })
endfunction
nnoremap z= :call FzfSpell()<CR>

" creates a new file in pwd with specific format
command! -nargs=1 NewZettel :execute ":e" . getcwd() . "/" . strftime("%Y%m%d%H%M") . "-<args>.md"
nnoremap <leader>nz :NewZettel 


function! ZettelFZF(file)
    "let filename = fnameescape(fnamemodify(a:file, ":t"))
    "why only the tail ?  I believe the whole filename must be linked unless everything is flat ...
    let filename = fnameescape(a:file)
    let filename_wo_timestamp = fnameescape(fnamemodify(a:file, ":t:s/^[0-9]*-//"))
     " Insert the markdown link to the file in the current buffer
    let mdlink = "[ ".filename_wo_timestamp." ]( ".filename." )"
    put=mdlink
endfunction

command! -nargs=1 ZettelFZF :call ZettelFZF(<f-args>)

" trigger backlink insert from insert mode
inoremap <buffer> <C-X><C-F> <esc>:call fzf#run({'source':'python ~/.vim/tools/NoteLister.py $(pwd)', 'sink': 'ZettelFZF'})<CR>

" update tags
nnoremap <leader>tt :silent !ctags -R . <CR>:redraw!<CR>
